# SensorPermission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**begins** | [**\DateTime**](\DateTime.md) |  | [optional] 
**can_modify** | **bool** |  | [optional] 
**deleted** | [**\DateTime**](\DateTime.md) |  | [optional] 
**ends** | [**\DateTime**](\DateTime.md) |  | [optional] 
**granted** | **string** |  | [optional] 
**granted_by** | **object** |  | [optional] 
**id** | **int** |  | [optional] 
**sensors** | [**\KiwiKi\Model\Sensor[]**](Sensor.md) |  | [optional] 
**timeofday_begins** | **string** |  | [optional] 
**timeofday_ends** | **string** |  | [optional] 
**timing_state** | **string** |  | [optional] 
**users** | **object[]** |  | [optional] 
**weekdays** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


