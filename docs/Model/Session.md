# Session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**session** | **object** |  | [optional] 
**session_key** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


