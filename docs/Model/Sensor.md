# Sensor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **object** |  | [optional] 
**can_invite** | **bool** |  | [optional] 
**highest_permission** | **string** |  | [optional] 
**is_owner** | **bool** |  | [optional] 
**owner** | **object** |  | [optional] 
**sensor_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


