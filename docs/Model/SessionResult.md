# SessionResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**\KiwiKi\Model\Session**](Session.md) |  | [optional] 
**status** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


