<?php
/**
 * Sensor
 *
 * PHP version 5
 *
 * @category Class
 * @package  KiwiKi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * The KIWI API
 *
 * This is the documentation for the HTTP API provided by KIWI.KI GmbH for remote unlocking and administration of doors. This API can be used for integration into existing systems and for application development.
 *
 * OpenAPI spec version: 1.0.0 (Stable)
 * Contact: api@kiwi.ki
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.27
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace KiwiKi\Model;

use \ArrayAccess;
use \KiwiKi\ObjectSerializer;

/**
 * Sensor Class Doc Comment
 *
 * @category Class
 * @package  KiwiKi
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Sensor implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'Sensor';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'address' => 'object',
        'can_invite' => 'bool',
        'highest_permission' => 'string',
        'is_owner' => 'bool',
        'owner' => 'object',
        'sensor_id' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'address' => null,
        'can_invite' => null,
        'highest_permission' => null,
        'is_owner' => null,
        'owner' => null,
        'sensor_id' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'address' => 'address',
        'can_invite' => 'can_invite',
        'highest_permission' => 'highest_permission',
        'is_owner' => 'is_owner',
        'owner' => 'owner',
        'sensor_id' => 'sensor_id'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'address' => 'setAddress',
        'can_invite' => 'setCanInvite',
        'highest_permission' => 'setHighestPermission',
        'is_owner' => 'setIsOwner',
        'owner' => 'setOwner',
        'sensor_id' => 'setSensorId'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'address' => 'getAddress',
        'can_invite' => 'getCanInvite',
        'highest_permission' => 'getHighestPermission',
        'is_owner' => 'getIsOwner',
        'owner' => 'getOwner',
        'sensor_id' => 'getSensorId'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['address'] = isset($data['address']) ? $data['address'] : null;
        $this->container['can_invite'] = isset($data['can_invite']) ? $data['can_invite'] : null;
        $this->container['highest_permission'] = isset($data['highest_permission']) ? $data['highest_permission'] : null;
        $this->container['is_owner'] = isset($data['is_owner']) ? $data['is_owner'] : null;
        $this->container['owner'] = isset($data['owner']) ? $data['owner'] : null;
        $this->container['sensor_id'] = isset($data['sensor_id']) ? $data['sensor_id'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets address
     *
     * @return object
     */
    public function getAddress()
    {
        return $this->container['address'];
    }

    /**
     * Sets address
     *
     * @param object $address address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->container['address'] = $address;

        return $this;
    }

    /**
     * Gets can_invite
     *
     * @return bool
     */
    public function getCanInvite()
    {
        return $this->container['can_invite'];
    }

    /**
     * Sets can_invite
     *
     * @param bool $can_invite can_invite
     *
     * @return $this
     */
    public function setCanInvite($can_invite)
    {
        $this->container['can_invite'] = $can_invite;

        return $this;
    }

    /**
     * Gets highest_permission
     *
     * @return string
     */
    public function getHighestPermission()
    {
        return $this->container['highest_permission'];
    }

    /**
     * Sets highest_permission
     *
     * @param string $highest_permission highest_permission
     *
     * @return $this
     */
    public function setHighestPermission($highest_permission)
    {
        $this->container['highest_permission'] = $highest_permission;

        return $this;
    }

    /**
     * Gets is_owner
     *
     * @return bool
     */
    public function getIsOwner()
    {
        return $this->container['is_owner'];
    }

    /**
     * Sets is_owner
     *
     * @param bool $is_owner is_owner
     *
     * @return $this
     */
    public function setIsOwner($is_owner)
    {
        $this->container['is_owner'] = $is_owner;

        return $this;
    }

    /**
     * Gets owner
     *
     * @return object
     */
    public function getOwner()
    {
        return $this->container['owner'];
    }

    /**
     * Sets owner
     *
     * @param object $owner owner
     *
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->container['owner'] = $owner;

        return $this;
    }

    /**
     * Gets sensor_id
     *
     * @return int
     */
    public function getSensorId()
    {
        return $this->container['sensor_id'];
    }

    /**
     * Sets sensor_id
     *
     * @param int $sensor_id sensor_id
     *
     * @return $this
     */
    public function setSensorId($sensor_id)
    {
        $this->container['sensor_id'] = $sensor_id;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


