# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact** | **object** |  | [optional] 
**friendly_name** | **string** |  | [optional] 
**organization_id** | **int** |  | [optional] 
**user_id** | **int** |  | [optional] 
**username** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


